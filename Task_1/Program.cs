﻿using System;

namespace Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int sumOdd = 0;
            int[] numMas = { 1,2,3,4,5 };

            for(int i=0; i < numMas.Length; i++)
            {
                if(numMas[i] %2 != 0)
                {
                    sumOdd += numMas[i];
                }
            }
            Console.WriteLine($"Сумма всех нечетных чисел равна: {sumOdd}");
        }
    }
}
